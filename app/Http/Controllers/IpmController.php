<?php namespace App\Http\Controllers;

use Config;
use Log;

use App\Models\Account;
use App\Models\AccountType;
use App\Models\Provider;
use App\Models\ProviderPrefix;
use App\Models\ProviderPrefixSubs;
use App\Models\ProviderSerie;
use App\Models\Payment;
use App\Models\CurlWrapper;
use App\Models\Common;

class IpmController extends Controller {

	public function __construct()
	{
	}

	public function addPayment()
	{

		\Log::info('[ADDPAYMENT] New payment: ' . \Request::get('request'));

		// common functions
		$common = new Common;

		// load IPM config
		$config = Config::get('ipm');

		// get arguments or die
		$json = json_decode(\Request::get('request'), true);
		$type = (empty($json['type']) ? 'AUTO' : strtoupper($json['type']));

		if (strlen($json['phone']) == 11)
		{
			$phone_raw 		= substr($json['phone'], 1);
		} else {
			$phone_raw 		= $json['phone'];
		}

		if (!isset($json['amount']) OR empty($json['amount']))
		{
			\Log::info('[ADDPAYMENT.ERROR] Error, missing required argument "amount". Check json structure.');
			echo json_encode(array('error' => 'Missing required argument "amount". Check json structure.'));
			die;
		}

		if (!isset($json['key']) OR empty($json['key']))
		{
			\Log::info('[ADDPAYMENT.ERROR] Error, missing required argument "key". Check json structure.');
			echo json_encode(array('error' => 'Missing required argument "key". Check json structure.'));
			die;
		}

		if ($type == "AUTO")
		{
			if (!isset($json['promo_payment_id']) OR empty($json['promo_payment_id']))
			{
				\Log::info('[ADDPAYMENT.ERROR] Error, missing required argument "promo_payment_id". Check json structure.');
				echo json_encode(array('error' => 'Missing required argument "promo_payment_id". Check json structure.'));
				die;
			}
		}
		

		$amount 				= $json['amount'];
		$key 					= strtoupper($json['key']);
		$parent_id				= (empty($json['parent_id']) ? '0' : $json['parent_id']);
		$promo_payment_id		= (empty($json['promo_payment_id']) ? '0' : $json['promo_payment_id']);


		// check phone number in prefix_subs table
		$phone = $common->check_prefix_sub($phone_raw);

		$check_provider = $common->set_provider($phone);
		
		if ($check_provider == 0)
		{
			\Log::info('[ADDPAYMENT.ERROR] Error, cant find provider. Check phone number. ' . \Request::get('request'));
			echo json_encode(array('error' => 'Cant find provider. Check phone number'));
			die;
		}

		// check for dub transaction
		$check_dp = 0;

		if ($promo_payment_id > 0)
		{
			$account_id_check = $common->get_account($key);
			$check_dp = Payment::where('account_id', $account_id_check)->where('phone_number', $phone)->where('promo_payment_id', $promo_payment_id)->first();

			if ($check_dp)
			{
				\Log::info('[ADDPAYMENT.ERROR] Error, payment duplication by "phone: ' . $phone . '" and "promo_payment_id: ' . $promo_payment_id . '". Returned old tx_number.');
				echo json_encode(array('tx_number' => $check_dp->tx_number));
				die;
			}
		}
		

		// prepare payment data
		$item = new Payment;
		$item->account_id			= $common->get_account($key);
		$item->provider_id			= $check_provider;
		$item->phone_number			= $phone;
		$item->amount 				= $amount;
		$item->payment_date			= time();
		$item->parent_id			= $parent_id;
		$item->type 				= $type;
		$item->promo_payment_id 	= $promo_payment_id;
		$item->save();

		// current payment id
		$current_payment_id = $item->id;
		// pad zero if id < 6 sym
		$pzlid = (strlen($current_payment_id) < 6) ? str_pad($current_payment_id, 6, "0", STR_PAD_LEFT) : $current_payment_id;
		// create tx_id
		$tx_id = "1" . date("ymdH") . substr(date("i"), 0, 1) . substr($pzlid, -6);
		// create rcp_id
		$rcp_id = date("y") . substr($pzlid, -6);
		// Составляем XML для платежа
		$source_xml = $common->create_payment_xml($tx_id, $rcp_id, $phone, $amount);

		$update = Payment::find($current_payment_id);
		$update->tx_number 		= $tx_id;
		$update->rcp_number 	= $rcp_id;
		$update->source_xml 	= $source_xml;
		$update->save();

		$result_arr = array('tx_number' => $tx_id);
		usleep(500);

		echo json_encode($result_arr);

	}

	public function newPayment()
	{

		\Log::info('[NEWPAYMENT] New payment: ' . \Request::get('request'));

		// common functions
		$common = new Common;

		// load IPM config
		$config = Config::get('ipm');

		// get arguments or die
		$json = json_decode(\Request::get('request'), true);
		//print_r($json);
		//echo "<br/>";

		$type = (empty($json['type']) ? 'AUTO' : strtoupper($json['type']));
		//echo $json['type'];
		//echo "<br/>";

		if (strlen($json['phone']) == 11)
		{
			$phone_raw 		= substr($json['phone'], 1);
		} else {
			$phone_raw 		= $json['phone'];
		}

		if (!isset($json['amount']) OR empty($json['amount']))
		{
			\Log::info('[NEWPAYMENT.ERROR] Error, missing required argument "amount". Check json structure.');
			echo json_encode(array('error' => 'Missing required argument "amount". Check json structure.'));
			die;
		}

		if (!isset($json['key']) OR empty($json['key']))
		{
			\Log::info('[NEWPAYMENT.ERROR] Error, missing required argument "key". Check json structure.');
			echo json_encode(array('error' => 'Missing required argument "key". Check json structure.'));
			die;
		}

		if ($type == "AUTO" || $type == "WALLET")
		{
			if (!isset($json['promo_payment_id']) OR empty($json['promo_payment_id']))
			{
				\Log::info('[NEWPAYMENT.ERROR] Error, missing required argument "promo_payment_id". Check json structure.');
				echo json_encode(array('error' => 'Missing required argument "promo_payment_id". Check json structure.'));
				die;
			}
		}
	

		$amount 				= $json['amount'];
		$key 					= strtoupper($json['key']);
		$parent_id				= (empty($json['parent_id']) ? '0' : $json['parent_id']);
		$promo_payment_id		= (empty($json['promo_payment_id']) ? '0' : $json['promo_payment_id']);


		if ($type != "WALLET") {
			// check phone number in prefix_subs table
			$phone = $common->check_prefix_sub($phone_raw);

			$check_provider = $common->set_provider($phone);
			
			if ($check_provider == 0)
			{
				\Log::info('[NEWPAYMENT.ERROR] Error, cant find provider. Check phone number. ' . \Request::get('request'));
				echo json_encode(array('error' => 'Cant find provider. Check phone number'));
				die;
			}
		} else {
			$phone = $phone_raw;
			$check_provider = $common->set_wallet_provider($key);

			if ($check_provider == 0)
			{
				\Log::info('[NEWPAYMENT.ERROR] Error, provider cant topup wallet. Check account type. ' . \Request::get('request'));
				echo json_encode(array('error' => 'Provider cant topup wallet. Check account type.'));
				die;
			}

		}
		
		// check for dub transaction
		$check_dp = 0;

		if ($promo_payment_id > 0)
		{
			$account_id_check = $common->get_account($key);
			$check_dp = Payment::where('account_id', $account_id_check)->where('phone_number', $phone)->where('promo_payment_id', $promo_payment_id)->first();

			if ($check_dp)
			{
				\Log::info('[NEWPAYMENT.ERROR] Error, payment duplication by "phone: ' . $phone . '" and "promo_payment_id: ' . $promo_payment_id . '". Returned old tx_number.');
				echo json_encode(array('tx_number' => $check_dp->tx_number));
				die;
			}
		}
			
		// prepare payment data
		$item = new Payment;
		$item->account_id			= $common->get_account($key);
		$item->provider_id			= $check_provider;
		$item->phone_number			= $phone;
		$item->amount 				= $amount;
		$item->payment_date			= time();
		$item->parent_id			= $parent_id;
		$item->type 				= $type;
		$item->promo_payment_id 	= $promo_payment_id;
		$item->save();

		// current payment id
		$current_payment_id = $item->id;
		// pad zero if id < 6 sym
		$pzlid = (strlen($current_payment_id) < 6) ? str_pad($current_payment_id, 6, "0", STR_PAD_LEFT) : $current_payment_id;
		// create tx_id
		$tx_id = "1" . date("ymdH") . substr(date("i"), 0, 1) . substr($pzlid, -6);
		// create rcp_id
		$rcp_id = date("y") . substr($pzlid, -6);

		if ($type == "WALLET") {
			// Составляем XML для платежа
			$source_xml = $common->create_wallet_payment_xml($tx_id, $rcp_id, $phone, $amount, $check_provider);
		} else {
			// Составляем XML для платежа
			$source_xml = $common->create_payment_xml($tx_id, $rcp_id, $phone, $amount);
		}

		$update = Payment::find($current_payment_id);
		$update->tx_number 		= $tx_id;
		$update->rcp_number 	= $rcp_id;
		$update->source_xml 	= $source_xml;
		$update->save();

		$result_arr = array('tx_number' => $tx_id);
		usleep(500);

		echo json_encode($result_arr);

	}

}
