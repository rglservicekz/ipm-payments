<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Config;
use Log;

use App\Models\Account;
use App\Models\Provider;
use App\Models\ProviderPrefix;
use App\Models\ProviderPrefixSubs;
use App\Models\ProviderSerie;
use App\Models\Payment;
use App\Models\CurlWrapper;

class RunApp extends Command {

	protected $name = 'payments-app';

	protected $description = 'Run payments application (all operations).';

	public function __construct()
	{
		parent::__construct();
	}

	public function fire()
	{

		Log::info('---------- [ Start payments app ' . date("Y-m-d H:i:s") . ' ] ----------');
		sleep(1);

		// run payments 
		$this->call('payments-run');

		// usleep before checking
		sleep(2);

		// run check 
		$this->call('payments-check');

		// usleep before reverse
		sleep(2);

		// run reverse 
		$this->call('payments-reverse');

		// usleep before reverse
		sleep(2);

		// run restart 
		$this->call('payments-error-restart');

		sleep(1);
		Log::info('----------- [ End payments app ' . date("Y-m-d H:i:s") . ' ] -----------');


	}

}
