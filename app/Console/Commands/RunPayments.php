<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Config;
use Log;

use App\Models\Account;
use App\Models\AccountType;
use App\Models\Provider;
use App\Models\ProviderPrefix;
use App\Models\ProviderPrefixSubs;
use App\Models\ProviderSerie;
use App\Models\Payment;
use App\Models\CurlWrapper;

class RunPayments extends Command {

	protected $name = 'payments-run';
	protected $description = 'Command starts to process all new payments.';

	public function __construct()
	{
		parent::__construct();
	}

	// run 
	public function fire()
	{
		// load IPM config
		$config = Config::get('ipm');

		//get active accounts & accounts with positive balance
		$accounts = Account::where('is_active', 1)->where('balance', '>', 0)->get();
		foreach($accounts as $account_unit)
		{
			$active_accounts[] = $account_unit->id;
		}

		$payments = Payment::whereIn('account_id', $active_accounts)->
							 whereNull('req_xml')->
							 whereNull('chk_xml')->
							 where('is_complete', 'N')->
							 where('retry_counter', 0)->
							 orderBy('id', 'asc')->
							 take(100)->
							 get();

		Log::info('Payments to process: ' . count($payments));

		$i = 1;

		// start foreach
		foreach ($payments as $payment) {

			$account = Account::find($payment->account_id);

			// check balance
			if ($account->balance < $payment->amount) 
			{
				Log::error($i . '. Payment ID:' . $payment->id . ' - Not enough money (' . $account->key . ': ' . $account->balance . '<' . $payment->amount .  ')');   
				$i++;
				continue; // go to next transaction if not enough money
			}
        
			// process payment
			$proc = new CurlWrapper;
			$result = $proc->exec_curl_post($config['url'], $config['timeout'], $payment->source_xml);
            $result_info = $result['info'];
            
            if ($result_info['http_code'] != '200')
            {
            	Log::error($i . '. Payment ID: ' . $payment->id . ' - FAILED! (http_code: ' . $result_info['http_code'] . '; total_time: ' . $result_info['total_time'] . ')');
            	$i++;
            	continue;
            }

            Log::info($i . '. Payment ID: ' . $payment->id . ' - SUCCESSFUL! (http_code: ' . $result_info['http_code'] . '; total_time: ' . $result_info['total_time'] . ')');            
        
            $result_xml = simplexml_load_string(stripslashes($result['response']));

			$item = Payment::find($payment->id);
			$item->req_xml			= $result['response'];
			$item->req_status		= $result_xml->payment['status'][0];
			$item->req_rc			= $result_xml->payment['result-code'][0];
			$item->req_fstatus		= $result_xml->payment['final-status'][0];
			$item->req_ferror		= $result_xml->payment['fatal-error'][0];
			$item->req_txn_id		= $result_xml->payment['txn-id'][0];
			$item->save();

			Account::where('id', $payment->account_id)->decrement('balance', $payment->amount);
			usleep(500);
			Account::where('id', $payment->account_id)->increment('locked', $payment->amount);

			$i++;

			// sleep before next transaction
			usleep(200000);

		}
		// end foreach

	}

	

}
