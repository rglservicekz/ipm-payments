<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Config;
use Log;

use App\Models\Account;
use App\Models\Provider;
use App\Models\ProviderPrefix;
use App\Models\ProviderPrefixSubs;
use App\Models\ProviderSerie;
use App\Models\Payment;
use App\Models\CurlWrapper;
use App\Models\Common;

class RestartErrorPayments extends Command {

	protected $name = 'payments-error-restart';

	protected $description = 'Restart payments which returned with Qiwi internal error (rc: 202; chk: 130-210)';

	public function __construct()
	{
		parent::__construct();
	}

	public function fire()
	{
		$time_limit = time() - (86400*10);

		// load IPM config
		$config = Config::get('ipm');

		//$payments = Payment::where('phone_number', '7018866444')->
		$payments = Payment::where('payment_date', '>', $time_limit)->
							 whereIn('chk_status', array('160', '130'))->
							 where('chk_rc', 210)->
							 where('chk_fstatus', 'true')->
							 where('chk_ferror', 'true')->
							 where('is_complete', 'LIKE', 'N')->
							 //take(50)->
							 get();

		Log::info('Payments to restart: ' . count($payments));

		// start foreach
		foreach ($payments as $payment) {

			// common functions
			$common = new Common;

			// load IPM config
			$config = Config::get('ipm');

			// clear payment data (restart)
			$item = Payment::find($payment->id);
			$item->req_xml				= null;
			$item->req_status			= 0;
			$item->req_rc				= 0;
			$item->req_fstatus 			= 'false';
			$item->req_ferror			= 'false';
			$item->req_txn_id			= 0;			
			$item->chk_xml				= null;
			$item->chk_status			= 0;
			$item->chk_rc				= 0;
			$item->chk_fstatus 			= 'false';
			$item->chk_ferror			= 'false';	
			$item->save();

			Log::info('Payment ID: ' . $payment->id . ' restarted!');            

			// sleep before next transaction
			usleep(250000);                                     

		}
		// end foreach
	
	}

}
