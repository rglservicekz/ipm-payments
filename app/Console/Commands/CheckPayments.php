<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Config;
use Log;

use App\Models\Account;
use App\Models\Provider;
use App\Models\ProviderPrefix;
use App\Models\ProviderPrefixSubs;
use App\Models\ProviderSerie;
use App\Models\Payment;
use App\Models\CurlWrapper;
use App\Models\Common;

class CheckPayments extends Command {

	protected $name = 'payments-check';
	protected $description = 'Check payments status.';

	public function __construct()
	{
		parent::__construct();
	}

	public function fire()
	{
		// load IPM config
		$config = Config::get('ipm');
		
		//$payments = Payment::where('phone_number', '7018866444')->
		//$payments = Payment::where('account_id', '!=', 57)->
		$payments = Payment::whereNotNull('req_xml')->
							 where('chk_fstatus', 'false')->
							 where('chk_ferror', 'false')->
							 where('is_complete', 'N')->
							 where('reverse_id', 0)->
							 orderBy('id', 'asc')->
							 take(100)->
							 get();

		Log::info('Payments to check: ' . count($payments));

		$i = 1;

		// start foreach
		foreach ($payments as $payment) {

			// common functions
			$common = new Common;
			//echo $common->create_check_xml($payment->tx_number);
			//die;

			// process check payment
			$proc = new CurlWrapper;
			$result = $proc->exec_curl_post($config['url'], $config['timeout'], $common->create_check_xml($payment->tx_number));
            $result_info = $result['info'];
            
            if ($result_info['http_code'] != '200')
            {
            	Log::error($i . '. Check payment ID: ' . $payment->id . ' - FAILED! (http_code: ' . $result_info['http_code'] . '; total_time: ' . $result_info['total_time'] . ')');
            	$i++;
            	continue;
            }

            Log::info($i . '. Check payment ID: ' . $payment->id . ' - SUCCESSFUL! (http_code: ' . $result_info['http_code'] . '; total_time: ' . $result_info['total_time'] . ')');

            $result_xml = simplexml_load_string(stripslashes($result['response']));

            $check_status = $common->check_status($result_xml->payment['status'][0], $result_xml->payment['result-code'][0], $payment->amount, $payment->account_id, $payment->retry_counter, $payment->id);

			$item = Payment::find($payment->id);
			$item->chk_xml			= $result['response'];
			$item->chk_status		= $result_xml->payment['status'][0];
			$item->chk_rc			= $result_xml->payment['result-code'][0];
			$item->chk_fstatus		= $result_xml->payment['final-status'][0];
			$item->chk_ferror		= $result_xml->payment['fatal-error'][0];
			$item->is_complete		= $check_status[0];
			$item->retry_counter	= $check_status[2];
			$item->finish_date		= $check_status[1];
			$item->save();  

			usleep(10000); 

			// update parent transaction (if this tx is reverse)
			if ($payment->type == 'REVERSE' && $check_status[0] == 'Y')
			{
				$parent_payment = Payment::find($payment->parent_id);
				$parent_payment->chk_status 	= $result_xml->payment['status'][0];
				$parent_payment->chk_rc 		= $result_xml->payment['result-code'][0];
				$parent_payment->chk_fstatus 	= $result_xml->payment['final-status'][0];
				$parent_payment->chk_ferror 	= $result_xml->payment['fatal-error'][0];
				$parent_payment->is_complete 	= $check_status[0];
				$parent_payment->finish_date 	= $check_status[1];
				$parent_payment->save();

				Log::info('Reverse status - Parent payment: ' . $payment->parent_id . ' - UPDATED! (Chind ID: ' . $payment->id . ')');
			}

			$i++;
			// sleep before next transaction
			usleep(200000);                                        

		}
		// end foreach

		
		$time_limit = time() - (86400*3);

		$qie_payments = Payment::where('chk_xml', 'LIKE', '%fatal="true">300<%')->
								 where('chk_fstatus', '')->
								 where('chk_ferror', '')->
								 where('is_complete', 'N')->
								 where('reverse_id', 0)->
								 orderBy('id', 'desc')->
								 where('payment_date', '>', $time_limit)->
								 get();

		Log::info('Payments to check (QIE): ' . count($qie_payments));

		$i = 1;

		// start foreach
		foreach ($qie_payments as $qie_payment) {

			// common functions
			$common = new Common;

			// process check payment
			$proc = new CurlWrapper;
			$result = $proc->exec_curl_post($config['url'], $config['timeout'], $common->create_check_xml($qie_payment->tx_number));
            $result_info = $result['info'];
            
            if ($result_info['http_code'] != '200')
            {
            	Log::error($i . '. Check (QIE) payment ID: ' . $qie_payment->id . ' - FAILED! (http_code: ' . $result_info['http_code'] . '; total_time: ' . $result_info['total_time'] . ')');
            	$i++;
            	continue;
            }

            Log::info($i . '. Check (QIE) payment ID: ' . $qie_payment->id . ' - SUCCESSFUL! (http_code: ' . $result_info['http_code'] . '; total_time: ' . $result_info['total_time'] . ')');

            $result_xml = simplexml_load_string(stripslashes($result['response']));

            $check_status = $common->check_status($result_xml->payment['status'][0], $result_xml->payment['result-code'][0], $qie_payment->amount, $qie_payment->account_id, $qie_payment->retry_counter, $qie_payment->id);

			$item = Payment::find($qie_payment->id);
			$item->chk_xml			= $result['response'];
			$item->chk_status		= $result_xml->payment['status'][0];
			$item->chk_rc			= $result_xml->payment['result-code'][0];
			$item->chk_fstatus		= $result_xml->payment['final-status'][0];
			$item->chk_ferror		= $result_xml->payment['fatal-error'][0];
			$item->is_complete		= $check_status[0];
			$item->retry_counter	= $check_status[2];
			$item->finish_date		= $check_status[1];
			$item->save();  

			usleep(10000); 

			$i++;
			// sleep before next transaction
			usleep(200000);                                        

		}
		// end foreach
		
	}

}
