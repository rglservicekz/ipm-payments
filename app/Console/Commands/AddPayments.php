<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Config;
use Log;

use App\Models\Account;
use App\Models\Provider;
use App\Models\ProviderPrefix;
use App\Models\ProviderPrefixSubs;
use App\Models\ProviderSerie;
use App\Models\Payment;
use App\Models\CurlWrapper;
use App\Models\Common;

class AddPayments extends Command {

	protected $name = 'payments-add';
	protected $description = 'Add new payment.';

	public function __construct()
	{
		parent::__construct();
	}

	public function fire()
	{
		// common functions
		$common = new Common;

		// load IPM config
		$config = Config::get('ipm');

		// get arguments or die
		$phone_raw 		= $this->argument('phone');
		$amount 		= $this->argument('amount');
		$key 			= strtoupper($this->argument('key'));
		$type			= ($this->argument('type') == '' ? 'AUTO' : strtoupper($this->argument('type')));
		$parent_id		= ($this->argument('parentid') == '' ? '0' : $this->argument('parentid'));

		// check phone number in prefix_subs table
		$phone = $common->check_prefix_sub($phone_raw);

		// prepare payment data
		$item = new Payment;
		$item->account_id			= $common->get_account($key);
		$item->provider_id			= $common->set_provider($phone);
		$item->phone_number			= $phone;
		$item->amount 				= $amount;
		$item->payment_date			= time();
		$item->parent_id			= $parent_id;
		$item->type 				= $type;
		$item->save();

		// current payment id
		$current_payment_id = $item->id;
		// pad zero if id < 6 sym
		$pzlid = (strlen($current_payment_id) < 6) ? str_pad($current_payment_id, 6, "0", STR_PAD_LEFT) : $current_payment_id;
		// create tx_id
		$tx_id = "1" . date("ymdH") . substr(date("i"), 0, 1) . substr($pzlid, -6);
		// create rcp_id
		$rcp_id = date("y") . substr($pzlid, -6);
		// Составляем XML для платежа
		$source_xml = $common->create_payment_xml($tx_id, $rcp_id, $phone, $amount);

		$update = Payment::find($current_payment_id);
		$update->tx_number 		= $tx_id;
		$update->rcp_number 	= $rcp_id;
		$update->source_xml 	= $source_xml;
		$update->save();

		echo $tx_id;	

	}

	protected function getArguments()
	{

		return [
			['phone', InputArgument::REQUIRED, 'First argument is Phone number.'],
			['amount', InputArgument::REQUIRED, 'Second argument is an Amount of payment.'],
			['key', InputArgument::REQUIRED, 'Third argument is Account key.'],
			['type', InputArgument::OPTIONAL, 'Fourth argument is Payment type AUTO/LIST/MANUAL.'],
			['parentid', InputArgument::OPTIONAL, 'Fifth argument is Parent ID.'],
		];
	}


}
