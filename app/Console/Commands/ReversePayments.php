<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Config;
use Log;

use App\Models\Account;
use App\Models\Provider;
use App\Models\ProviderPrefix;
use App\Models\ProviderPrefixSubs;
use App\Models\ProviderSerie;
use App\Models\Payment;
use App\Models\CurlWrapper;
use App\Models\Common;

class ReversePayments extends Command {

	protected $name = 'payments-reverse';

	protected $description = 'Check and run reverse payments.';

	public function __construct()
	{
		parent::__construct();
	}

	public function fire()
	{
		$time_limit = time() - (86400*15);

		// load IPM config
		$config = Config::get('ipm');

		//$payments = Payment::where('phone_number', '7018866444')->
		$payments = Payment::whereIn('provider_id', array('643', '644'))->
							 where('payment_date', '>', $time_limit)->
							 whereIn('chk_status', array('160', '130'))->
							 where('chk_rc', 5)->
							 where('is_complete', 'LIKE', 'N')->
							 where('reverse_id', 0)->
							 where('type', '!=', 'REVERSE')->
							 get();

		Log::info('Payments to reverse: ' . count($payments));

		// start foreach
		foreach ($payments as $payment) {

			// common functions
			$common = new Common;

			// load IPM config
			$config = Config::get('ipm');

			// get arguments or die
			$phone_raw 		= $payment->phone_number;
			$amount 		= $payment->amount;
			$account_id		= $payment->account_id;
			$type			= 'REVERSE';
			if ($payment->parent_id == 0) {
				$parent_id		= $payment->id;
			} else {
				$parent_id		= $payment->parent_id;
			}

			// check phone number in prefix_subs table
			$phone = $common->check_prefix_sub($phone_raw);

			// prepare payment data
			$item = new Payment;
			$item->account_id			= $account_id;
			$item->provider_id			= $common->set_reverse_provider($payment->provider_id);
			$item->phone_number			= $phone;
			$item->amount 				= $amount;
			$item->payment_date			= time();
			$item->parent_id			= $parent_id;
			$item->type 				= $type;
			$item->save();

			// current payment id
			$current_payment_id = $item->id;
			// pad zero if id < 6 sym
			$pzlid = (strlen($current_payment_id) < 6) ? str_pad($current_payment_id, 6, "0", STR_PAD_LEFT) : $current_payment_id;
			// create tx_id
			$tx_id = "1" . date("ymdH") . substr(date("i"), 0, 1) . substr($pzlid, -6);
			// create rcp_id
			$rcp_id = date("y") . substr($pzlid, -6);
			// Составляем XML для платежа
			$source_xml = $common->create_reverse_xml($tx_id, $rcp_id, $phone, $amount, $payment->provider_id);

			$update = Payment::find($current_payment_id);
			$update->tx_number 		= $tx_id;
			$update->rcp_number 	= $rcp_id;
			$update->source_xml 	= $source_xml;
			$update->save();

			usleep(500);
			$update_parent = Payment::find($payment->id);
			$update_parent->reverse_id = $current_payment_id;
			$update_parent->save();

			Log::info('Reverse to payment ID: ' . $payment->id . ' added! Reverse payment ID: ' . $current_payment_id . '.');            

			// sleep before next transaction
			usleep(250000);                                        

		}
		// end foreach
	}

}
