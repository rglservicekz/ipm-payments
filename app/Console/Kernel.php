<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\RunPayments',
		'App\Console\Commands\CheckPayments',
		'App\Console\Commands\AddPayments',
		'App\Console\Commands\ReversePayments',
		'App\Console\Commands\RestartErrorPayments',
		'App\Console\Commands\RunApp',
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		//
	}

}
