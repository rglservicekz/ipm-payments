<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurlWrapper {

	public function exec_curl_payment($phone, $amount, $acc_key, $type = 'MANUAL', $parent_id = '0') 
	{
		$curl = '';

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, 'http://coreserver.rglservice.kz/siesta2/ipm/pay.php?phone='.$phone.'&amount='.$amount.'&key='.$acc_key.'&type='. $type . '&parent_id=' . $parent_id);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_TIMEOUT, 60);
		$out = curl_exec($curl);
		$Result = $out;
		curl_close($curl);

		return $Result;
	}

	public function exec_curl_post($url, $timeout, $xml) 
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: text/xml;charset=UTF-8;"));
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);

		$result = array();
        $result['response'] = curl_exec($ch);
        $result['info'] = curl_getinfo($ch);

		return $result;
	}

		
}