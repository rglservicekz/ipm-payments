<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;
use Log;

class Common {

	public function get_account($key)
	{
		$account = Account::where('key', $key)->first();
		
		if ($account == true) {
			$result = $account->id;
		} else {
			$result = false;
		}

		return $result;

	}

	public function get_sign($tx_id)
	{
		// load IPM config
		$config = Config::get('ipm');

		$password = strtoupper(md5($config['password']));
		$tx_sum = strtoupper(md5($tx_id . $password));

		return strtoupper(md5($config['login'] . $password . $config['terminal_id'] . $config['request_type'] . $tx_sum));
	}

	public function check_prefix_sub($phone)
	{
		
		$subs = ProviderPrefixSubs::all();
		
		foreach ($subs as $sub) {
			$check_sub= preg_match('/^'.$sub->prefix_ogn.'/', $phone, $matches);
			if ($check_sub === 1)
			{
				$sub_prefix_phone = $sub->prefix_sub . substr($phone, strlen($sub->prefix_ogn));
				break;
			} else {
				$sub_prefix_phone = $phone;
			}
		}

		return $sub_prefix_phone;

	}

	public function check_status($check_status, $check_rc, $amount, $account_id, $retry, $payment_id)
	{
		$is_complete = 'N';
		$finish_date = '0';
		$retry_counter = $retry;

	
		if (($check_status == "51" || $check_status == "60" || $check_status == "61") && $check_rc == "0") 
		{
			// payment successful
			Account::where('id', $account_id)->decrement('locked', $amount);

			// result
			$is_complete = 'Y';
			$finish_date = time();

			Log::info('Check status - payment ID: ' . $payment_id . ' - COMPLETED! (is_complete: ' . $is_complete . '; decremented from locked: ' . $amount . ' tg.)');

		} 
		else 
		{
			// payment failed
			if (($check_status == "130" || $check_status == "160") && ($check_rc == "210" || $check_rc == "5" || $check_rc == "300" || $check_rc == "202")) 
			{
				// payment failed / transaction not found
				// return money from locked to balance
				Account::where('id', $account_id)->decrement('locked', $amount);
				usleep(100);
				Account::where('id', $account_id)->increment('balance', $amount);

				Log::error('Check status - payment ID: ' . $payment_id . ' - RETURNED! (is_complete: ' . $is_complete . '; returned to balance: ' . $amount . ' tg.)');

			}
		}

		// return result
		return array( $is_complete, $finish_date, $retry_counter );
	}
    
    public function _check_mnp($phone)
    {
        $user_phone = '7' . $phone;
        
        $url = 'http://coreserver.rglservice.kz:8080/mnp/owners?phone=' . $user_phone;
        $check_status = @file_get_contents($url, true);

        if ($check_status === false)
        {
        	$foperator = 0; 
        	// die ('Could not connect to MNP server.');

        } else {

	        $check_obj = json_decode($check_status);

	        if ($check_obj->status == 'true') {
	        	$mnp_operator = $check_obj->siesta_smsc_id;
		        
		        if ($mnp_operator == 'smsc-kcell') {
		           $foperator = '643'; 
		        } elseif ($mnp_operator == 'smsc-beeline') {
		           $foperator = '142'; 
		        } elseif ($mnp_operator == 'smsc-altel') {
		           $foperator = '240'; 
		        } elseif ($mnp_operator == 'smsc-tele2') {
		           $foperator = '431'; 
		        } else {
		           $foperator = 0; 
		        }

	        } else {
	        	$foperator = 0; 
	        }
	    }
               
        return $foperator;
        
        
    }

	public function set_provider($phone)
	{
		$result = 0;
		
		// check mnp
		$check_mnp_status = $this->_check_mnp($phone);

		if ($check_mnp_status != 0) {

			$result = $check_mnp_status;

		} else {

			$user_prefix = substr($phone, 0, 3);
			$user_serie = substr($phone, 0, 6);

			$check_serie = ProviderSerie::where('serie', $user_serie)->first();
		
			if ($check_serie == true) {

				$provider = Provider::find($check_serie->provider_id);
				$result = $provider->osmp_code;	

			} else {

				$prefixes = ProviderPrefix::orderBy(\DB::raw('ABS(prefix)'), 'desc')->get();
				//$prefixes = ProviderPrefix::orderBy(DB::raw('ABS(prefix)'), 'desc')->get();
			
				foreach ($prefixes as $prefix) {
					$check_prefix = preg_match('/^'.$prefix->prefix.'/', $phone, $matches);
					if ($check_prefix === 1)
					{
						$provider = Provider::find($prefix->provider_id);
						$result = $provider->osmp_code;	
						break;
					}
				}

			}
		}

		return $result;
	}

	public function set_reverse_provider($osmp_code)
	{
		$provider = Provider::where('osmp_code', $osmp_code)->first();

		if ($provider) {
			$result = $provider->osmp_reverse;
		} else {
			$result = false;
		}

		return $result;

	}

	public function set_wallet_provider($key)
	{
		$result = 0;
		
		$account = Account::where('key', $key)->first();
		$account_type = AccountType::find($account->type);

		if ($account_type->balance_topup == 1 && $account_type->wallet_topup == 1) {
			$provider = Provider::find($account_type->wallet_provider_id);
			$result = $provider->osmp_code;
		} else if ($account_type->balance_topup == 0 && $account_type->wallet_topup == 1) {
			$provider = Provider::find($account_type->wallet_provider_id);
			$result = $provider->osmp_code;
		} else {
			$result = 0;
		}

		return $result;
	}

	public function create_payment_xml($tx_id, $rcp_id, $phone, $amount)
	{

		// load IPM config
		$config = Config::get('ipm');

		$xml = new \SimpleXMLElement('<request/>');
		$item = $xml->addChild('protocol-version', '4.00');
		$item = $xml->addChild('request-type', $config['request_type']);
		$item = $xml->addChild('terminal-id', $config['terminal_id']);

		$extra = $xml->addChild('extra', $config['login']);
		$extra->addAttribute('name', 'login');

		$extra = $xml->addChild('extra', $this->get_sign($tx_id));
		$extra->addAttribute('name', 'sign-md5');

		$extra = $xml->addChild('extra', 'Dealer v1.9');
		$extra->addAttribute('name', 'client-software');

		$auth = $xml->addChild('auth');
			$auth->addAttribute('count', '1');
			$auth->addAttribute('to-amount', $amount . '.00');

			$payment = $auth->addChild('payment');
				$payment->addChild('transaction-number', $tx_id);
				$from = $payment->addChild('from');
					$from->addChild('amount', $amount . '.00');
				$to = $payment->addChild('to');
					$to->addChild('amount', $amount . '.00');
					$to->addChild('service-id', $this->set_provider($phone));
					$to->addChild('account-number', $phone);
				$receipt = $payment->addChild('receipt');
					$receipt->addChild('datetime', date("YmdHis", time()));
					$receipt->addChild('receipt-number', $rcp_id);

		return $xml->asXML();

	}

	public function create_wallet_payment_xml($tx_id, $rcp_id, $phone, $amount, $provider)
	{

		// load IPM config 
		$config = Config::get('ipm');

		$xml = new \SimpleXMLElement('<request/>');
		$item = $xml->addChild('protocol-version', '4.00');
		$item = $xml->addChild('request-type', $config['request_type']);
		$item = $xml->addChild('terminal-id', $config['terminal_id']);

		$extra = $xml->addChild('extra', $config['login']);
		$extra->addAttribute('name', 'login');

		$extra = $xml->addChild('extra', $this->get_sign($tx_id));
		$extra->addAttribute('name', 'sign-md5');

		$extra = $xml->addChild('extra', 'Dealer v1.9');
		$extra->addAttribute('name', 'client-software');

		$auth = $xml->addChild('auth');
			$auth->addAttribute('count', '1');
			$auth->addAttribute('to-amount', $amount . '.00');

			$payment = $auth->addChild('payment');
				$payment->addChild('transaction-number', $tx_id);
				$from = $payment->addChild('from');
					$from->addChild('amount', $amount . '.00');
				$to = $payment->addChild('to');
					$to->addChild('amount', $amount . '.00');
					$to->addChild('service-id', $provider);
					$to->addChild('account-number', $phone);
				$receipt = $payment->addChild('receipt');
					$receipt->addChild('datetime', date("YmdHis", time()));
					$receipt->addChild('receipt-number', $rcp_id);

		return $xml->asXML();

	}


	public function create_check_xml($tx_id)
	{
		// load IPM config
		$config = Config::get('ipm');

		$xml = new \SimpleXMLElement('<request/>');
		$item = $xml->addChild('protocol-version', '4.00');
		$item = $xml->addChild('request-type', $config['request_type']);
		$item = $xml->addChild('terminal-id', $config['terminal_id']);

		$extra = $xml->addChild('extra', $config['login']);
		$extra->addAttribute('name', 'login');

		$extra = $xml->addChild('extra', $this->get_sign($tx_id));
		$extra->addAttribute('name', 'sign-md5');

		$extra = $xml->addChild('extra', 'Dealer v1.9');
		$extra->addAttribute('name', 'client-software');

		$status = $xml->addChild('status');
			$payment = $status->addChild('payment');
			$payment->addChild('transaction-number', $tx_id);

		return $xml->asXML();
	}

	public function create_reverse_xml($tx_id, $rcp_id, $phone, $amount, $osmp_code)
	{

		// load IPM config
		$config = Config::get('ipm');

		$xml = new \SimpleXMLElement('<request/>');
		$item = $xml->addChild('protocol-version', '4.00');
		$item = $xml->addChild('request-type', $config['request_type']);
		$item = $xml->addChild('terminal-id', $config['terminal_id']);

		$extra = $xml->addChild('extra', $config['login']);
		$extra->addAttribute('name', 'login');

		$extra = $xml->addChild('extra', $this->get_sign($tx_id));
		$extra->addAttribute('name', 'sign-md5');

		$extra = $xml->addChild('extra', 'Dealer v1.9');
		$extra->addAttribute('name', 'client-software');

		$auth = $xml->addChild('auth');
			$auth->addAttribute('count', '1');
			$auth->addAttribute('to-amount', $amount . '.00');

			$payment = $auth->addChild('payment');
				$payment->addChild('transaction-number', $tx_id);
				$from = $payment->addChild('from');
					$from->addChild('amount', $amount . '.00');
				$to = $payment->addChild('to');
					$to->addChild('amount', $amount . '.00');
					$to->addChild('service-id', $this->set_reverse_provider($osmp_code));
					$to->addChild('account-number', $phone);
				$receipt = $payment->addChild('receipt');
					$receipt->addChild('datetime', date("YmdHis", time()));
					$receipt->addChild('receipt-number', $rcp_id);

		return $xml->asXML();

	}

}
