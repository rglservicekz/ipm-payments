#!/bin/bash

DATE=$(date --date="tomorrow" +%F)

touch /var/www/siesta2/payments/storage/logs/laravel-$DATE.log
chown -R www-data:www-data /var/www/siesta2/payments/storage/logs/
